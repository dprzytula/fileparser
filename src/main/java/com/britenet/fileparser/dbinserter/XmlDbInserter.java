package com.britenet.fileparser.dbinserter;

import com.britenet.fileparser.connection.DataSource;
import com.britenet.fileparser.model.Contact;
import com.britenet.fileparser.model.Person;
import com.britenet.fileparser.parser.FileParser;
import com.britenet.fileparser.utils.ContactType;
import lombok.EqualsAndHashCode;

import java.io.File;
import java.util.List;

public class XmlDbInserter implements FileDbInserter {
    private FileParser<Person> parser;
    private String path;

    public XmlDbInserter(String path, FileParser<Person> parser) {
        this.parser = parser;
        this.path = path;
    }

    public void run() {
        File file = new File(path);
        List<Person> rows = parser.unmarshall(file);
        int personId = 0;
        for(Person row : rows) {
            personId = DataSource.Person.insert(
                    row.getName(),
                    row.getSurname(),
                    row.getAge());
            if(personId != 0)
                for (Contact contact : row.getContacts().getContacts())
                    DataSource.Contact.insert(
                            personId,
                            ContactType.getEnumFromString(contact.getType()).getValue(),
                            contact.getContact());
        }
    }


}
