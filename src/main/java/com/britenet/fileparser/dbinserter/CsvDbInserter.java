package com.britenet.fileparser.dbinserter;

import com.britenet.fileparser.connection.DataSource;
import com.britenet.fileparser.parser.FileParser;
import com.britenet.fileparser.utils.ContactType;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.File;
import java.util.List;

public class CsvDbInserter implements FileDbInserter {
    private FileParser<String[]> parser;
    private String path;

    public CsvDbInserter(String path, FileParser<String[]> parser) {
        this.parser = parser;
        this.path = path;
    }

    public void run() {
        File file = new File(path);
        List<String[]> rows = parser.unmarshall(file);
        int personId = 0;
        for (String[] row : rows) {
            personId = DataSource.Person.insert(
                    row[0],
                    row[1],
                    NumberUtils.toInt(row[2]));
            //row[3]->city: omitted because it's redundant
            if(personId != 0)
                for (int i = 4; i < row.length; i++) {
                    DataSource.Contact.insert(
                            personId,
                            ContactType.deduceEnumFromString(row[i]).getValue(),
                            row[i]);
                }
        }
    }
}
