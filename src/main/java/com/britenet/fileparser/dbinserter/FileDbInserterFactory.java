package com.britenet.fileparser.dbinserter;

import com.britenet.fileparser.parser.CSVFileParser;
import com.britenet.fileparser.parser.XMLFileParser;

import java.util.Optional;

public class FileDbInserterFactory {

    public static FileDbInserter createFileDbInserter(String path) {
        //Get the file extension
        Optional<String> extension = Optional.ofNullable(path)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(path.lastIndexOf(".") + 1).toLowerCase());

        String ext = extension.orElseThrow(() -> new IllegalArgumentException("Invalid file path."));
        if(ext.equals("xml"))
            return new XmlDbInserter(path, new XMLFileParser());
        else if(ext.equals("txt") || ext.equals("csv"))
            return new CsvDbInserter(path, new CSVFileParser());
        else
            throw new IllegalArgumentException("Wrong file extension: " + ext);
    }
}
