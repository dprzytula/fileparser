package com.britenet.fileparser.utils;

import java.util.HashMap;
import java.util.Map;

public enum ContactType {
    unknown(0),
    email(1),
    phone(2),
    jabber(3);

    private int value;
    private static Map map = new HashMap<>();

    ContactType(int value) {
        this.value = value;
    }

    static {
        for (ContactType contactType : ContactType.values()) {
            map.put(contactType.value, contactType);
        }
    }

    public static ContactType getEnumFromString(String value) {
        try {
            return Enum.valueOf(ContactType.class, value.toLowerCase());
        }   catch(IllegalArgumentException ex) { }
        return ContactType.unknown;
    }

    public static ContactType deduceEnumFromString(String value) {
        value = value.toLowerCase();
        if(value.startsWith("jbr"))
            return ContactType.jabber;
        else if(value.contains("@"))
            return ContactType.email;
        else if(value.matches("/\\(?([0-9]{3})\\)?([ .-]?)([0-9]{3})\\2([0-9]{4})/"))
            return ContactType.phone;
        else
            return ContactType.unknown;
    }

    public static ContactType valueOf(int contactType) {
        return (ContactType) map.get(contactType);
    }

    public int getValue() {
        return value;
    }
}
