package com.britenet.fileparser.gui;

import com.britenet.fileparser.connection.DataSource;
import com.britenet.fileparser.dbinserter.FileDbInserter;
import com.britenet.fileparser.dbinserter.FileDbInserterFactory;
import javax.swing.*;
import java.awt.*;

public class FileChooserGUI extends JFrame{
    private String filePath;

    private final JLabel label;
    private final JLabel labelDbusername;
    private final JLabel labelDbpassword;
    private final JLabel labelDbname;
    private final JTextField dbusername;
    private final JTextField dbpassword;
    private final JTextField dbname;
    private final JButton bt;
    private final JButton btConnect;

    public FileChooserGUI() {
        super();
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        setLocationRelativeTo(null);
        setPreferredSize(new Dimension(210, 250));
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        label = new JLabel();
        labelDbusername = new JLabel("Nazwa użytkownika");
        labelDbpassword = new JLabel("Hasło użytkownika");
        labelDbname = new JLabel("Nazwa bazy danych");
        dbusername = new JTextField();
        dbpassword = new JTextField();
        dbname = new JTextField();
        bt = new JButton("Wybierz plik");
        btConnect = new JButton("Połącz");

        bt.addActionListener(e -> {
           JButton open = new JButton();
           JFileChooser fc = new JFileChooser();
           fc.setDialogTitle("Wybierz plik:");
           fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
           if (fc.showOpenDialog(open) == JFileChooser.APPROVE_OPTION) {
               filePath = fc.getSelectedFile().getAbsolutePath();
               try {
                   //Creating a proper FileDbInserter based on the file path
                   FileDbInserter fileDbInserter = FileDbInserterFactory.createFileDbInserter(filePath);
                   //Migration from file to database
                   fileDbInserter.run();
                   label.setForeground(new Color(10, 112, 46));
                   label.setText("Migracja zakończona.");
               } catch (Exception ex) {
                   label.setForeground(Color.RED);
                   label.setText("Wystapil blad.");
               }
           }
        });

        btConnect.addActionListener(e -> {
            if(DataSource.connect(dbusername.getText(), dbpassword.getText(), dbname.getText()) != null) {
                bt.setEnabled(true);
                label.setForeground(new Color(201, 151, 12));
                label.setText("Ustanowiono połączenie z bazą");
            } else {
                label.setForeground(Color.RED);
                label.setText("Brak połączenia z bazą");
                bt.setEnabled(false);
            }
        });


        //Adding components to layout
        add(labelDbusername);
        add(dbusername);
        add(labelDbpassword);
        add(dbpassword);
        add(labelDbname);
        add(dbname);
        add(btConnect);
        add(bt);
        add(label);

        //Setting components alignment
        btConnect.setAlignmentX(Component.CENTER_ALIGNMENT);
        btConnect.setAlignmentX(Component.CENTER_ALIGNMENT);
        bt.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelDbusername.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelDbpassword.setAlignmentX(Component.CENTER_ALIGNMENT);
        labelDbname.setAlignmentX(Component.CENTER_ALIGNMENT);

        if(DataSource.getConnection() == null) {
            label.setText("Brak połączenia z bazą");
            bt.setEnabled(false);
        }

        pack();
        setVisible(true);
    }
}
