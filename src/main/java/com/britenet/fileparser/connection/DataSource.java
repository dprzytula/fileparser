package com.britenet.fileparser.connection;

import java.sql.*;

public class DataSource {
    private static Connection con = null;

    private static final String CREATE_CUSTOMER = "CREATE TABLE IF NOT EXISTS customers (" +
            "  id int NOT NULL AUTO_INCREMENT," +
            "  name varchar(45) NOT NULL," +
            "  surname varchar(45) NOT NULL," +
            "  age int DEFAULT NULL," +
            "  PRIMARY KEY (id)) ";

    private static final String CREATE_CONTACT = "CREATE TABLE IF NOT EXISTS contacts (" +
            "  id int NOT NULL AUTO_INCREMENT," +
            "  id_customer int NOT NULL," +
            "  type int DEFAULT NULL," +
            "  contact varchar(100) DEFAULT NULL," +
            "  PRIMARY KEY (id)," +
            "  CONSTRAINT contacts_customer_fk FOREIGN KEY (id_customer) REFERENCES customers (id))";

    public static Connection connect(String username, String password, String dbname) {
        try {
            String DB_URL = "jdbc:mysql://localhost:3306/%s?serverTimezone=UTC";
            con = DriverManager.getConnection(
                    String.format(DB_URL, dbname),
                    username,
                    password);
            Statement stmt = con.createStatement();
            stmt.executeUpdate(CREATE_CUSTOMER);
            stmt.executeUpdate(CREATE_CONTACT);
            stmt.close();
            return con;
        } catch (SQLException e) {
            return null;
        }
    }

    public static Connection getConnection() {
        return con;
    }

    public static class Person {
        public static int insert(String name, String surname, Integer age){
            int id=0;
            try(PreparedStatement pstmt = con.prepareStatement(
                    "INSERT INTO customers(name, surname, age) " +
                            "VALUES(?, ?, ?)", Statement.RETURN_GENERATED_KEYS)){

                pstmt.setString(1, name);
                pstmt.setString(2, surname);
                if(age == null || age == 0)
                    pstmt.setNull(3, java.sql.Types.INTEGER);
                else
                    pstmt.setInt(3, age);
                pstmt.execute();
                ResultSet rs = pstmt.getGeneratedKeys();
                if(rs.next()) {
                    id = rs.getInt(1);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return id;
        }
    }

    public static class Contact{
        public static int insert(int id_customer, Integer type, String contact) {
            int id=0;
            try(PreparedStatement pstmt = con.prepareStatement(
                    "INSERT INTO contacts(id_customer,type,contact) " +
                            "VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
                pstmt.setInt(1, id_customer);
                pstmt.setInt(2, type);
                pstmt.setString(3, contact);
                pstmt.execute();
                ResultSet rs = pstmt.getGeneratedKeys();
                if(rs.next()) {
                    id = rs.getInt(1);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return id;
        }
    }

    private DataSource() {}
}
