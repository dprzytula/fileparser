package com.britenet.fileparser.parser;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CSVFileParser extends FileParser<String[]> {

    @Override
    public List<String[]> unmarshall(File file){
        List<String[]> list = new ArrayList<>();
        try {
            InputStream in = new FileInputStream(file);
            InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
            CSVReader csvReader = new CSVReader(reader);
            list = csvReader.readAll();
            reader.close();
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public CSVFileParser() {
    }

}
