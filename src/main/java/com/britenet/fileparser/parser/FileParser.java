package com.britenet.fileparser.parser;

import java.io.File;
import java.util.List;

public abstract class FileParser<T> {
     public abstract List<T> unmarshall(File path);
}
