package com.britenet.fileparser.parser;

import com.britenet.fileparser.model.Person;
import com.britenet.fileparser.model.Persons;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

public class XMLFileParser extends FileParser<Person> {

    @Override
    public List<Person> unmarshall(File file){
        List<Person> personList = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Persons.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            Persons persons = (Persons) jaxbUnmarshaller.unmarshal(file);

            personList = persons.getPeople();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return personList;
    }

    public XMLFileParser() {

    }
}
