package com.britenet.fileparser.parser;

import com.britenet.fileparser.model.Person;
import org.junit.Assert;
import java.io.File;
import java.util.List;
import org.junit.Test;

public class FileParserTest {

    @Test
    public void shouldReturnListOfStringWhenDataAreValid(){
        File initialFile = new File("src/main/resources/dane-osoby.txt");
        List<String[]> list = new CSVFileParser().unmarshall(initialFile);
        Assert.assertFalse(list.isEmpty());
    }

    @Test
    public void shouldReturnListOfPeopleWhenDataAreValid(){
        File initialFile = new File("src/main/resources/dane-osoby.xml");
        List<Person> list = new XMLFileParser().unmarshall(initialFile);
        Assert.assertFalse(list.isEmpty());
    }
}
