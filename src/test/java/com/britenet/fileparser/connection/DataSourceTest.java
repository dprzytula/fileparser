package com.britenet.fileparser.connection;

import org.junit.Test;
import org.junit.Assert;
import java.sql.Connection;

public class DataSourceTest {

    @Test
    public void shouldReturnNullWhenWrongCredentials() {
        Connection connection = DataSource.connect("u", "p", "b");
        Assert.assertNull(connection);
    }

}
