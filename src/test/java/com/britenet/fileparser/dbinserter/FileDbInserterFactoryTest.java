package com.britenet.fileparser.dbinserter;

import org.junit.Assert;
import org.junit.Test;

public class FileDbInserterFactoryTest {

    @Test
    public void shouldReturnXmlDbInserterWhenXMLExtension() {
        FileDbInserter fileDbInserter = FileDbInserterFactory.createFileDbInserter("file.xml");
        Assert.assertTrue(fileDbInserter instanceof XmlDbInserter);
    }

    @Test
    public void shouldReturnXmlDbInserterWhenCSVExtension() {
        FileDbInserter fileDbInserter = FileDbInserterFactory.createFileDbInserter("file.csv");
        Assert.assertTrue(fileDbInserter instanceof CsvDbInserter);
    }

    @Test
    public void shouldThrowExceptionWhenInvalidPath() {
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            FileDbInserter fileDbInserter = FileDbInserterFactory.createFileDbInserter("");
        });
    }
}
